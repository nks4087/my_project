<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220210154159 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE paises (id INT AUTO_INCREMENT NOT NULL, creado_en_pais DATETIME NOT NULL, actualizado_en_pais DATETIME NOT NULL, borrado_en_pais DATETIME DEFAULT NULL, codigo_pais VARCHAR(2) NOT NULL, nombre_pais VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, borrado_en_rol DATETIME DEFAULT NULL, creado_en_rol DATETIME NOT NULL, actualizado_en_rol DATETIME NOT NULL, rol_rol VARCHAR(255) NOT NULL, descripcion_rol VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuarios_usu (id INT AUTO_INCREMENT NOT NULL, paises_id INT NOT NULL, creado_en_usu DATETIME NOT NULL, actualizado_en_usu DATETIME NOT NULL, borrado_en_usu DATETIME DEFAULT NULL, email_usu VARCHAR(180) NOT NULL, pass_usu VARCHAR(255) NOT NULL, permitir_usu BOOLEAN DEFAULT false, nombre_usu VARCHAR(255) NOT NULL, apellidos_usu VARCHAR(255) DEFAULT NULL, tipo_usu VARCHAR(255) DEFAULT NULL, creado_por VARCHAR(255) DEFAULT NULL, actualizado_por VARCHAR(255) DEFAULT NULL, roles_usu JSON NOT NULL, UNIQUE INDEX UNIQ_A9267DC0C2FC69 (email_usu), INDEX IDX_A9267DC0282BEA6A (paises_id), PRIMARY KEY(id), FOREIGN KEY (paises_id) REFERENCES paises(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE usuarios_usu ADD CONSTRAINT FK_A9267DC0282BEA6A FOREIGN KEY (paises_id) REFERENCES paises (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE usuarios_usu DROP FOREIGN KEY FK_A9267DC0282BEA6A');
        $this->addSql('DROP TABLE paises');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE usuarios_usu');
    }
}
